package org.bigolin.bigoflix;

import org.bigolin.model.Filme;
import org.bigolin.model.Lista;

public class BigoflixTeste {

    public static void main(String[] args) {
        Lista preferidos = new Lista(10);
        
        Filme f1 = new Filme();
        f1.setNome("O Senhor dos Anéis: O Retorno do Rei");//Método pertence a midia
        f1.setDiretor("Peter Jackson"); //Método pertence a filme
        f1.setDuracao(38000);
        
        preferidos.add(f1);      
    }    
}
